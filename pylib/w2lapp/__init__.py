# -*- coding: utf-8 -*-
"""
w2lapp - module package of web2ldap application

web2ldap - a web-based LDAP Client,
see http://www.web2ldap.de for details

(c) by Michael Stroeder <michael@stroeder.com>

This module is distributed under the terms of the
GPL (GNU GENERAL PUBLIC LICENSE) Version 2
(see http://www.gnu.org/copyleft/gpl.html)
"""

# This is also the release's overall version number
__version__ = '1.2.97'
