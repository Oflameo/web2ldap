# -*- coding: utf-8 -*-
"""
w2lapp.ldapparams: Display (SSL) connection data

web2ldap - a web-based LDAP Client,
see http://www.web2ldap.de for details

(c) by Michael Stroeder <michael@stroeder.com>

This module is distributed under the terms of the
GPL (GNU GENERAL PUBLIC LICENSE) Version 2
(see http://www.gnu.org/copyleft/gpl.html)
"""

import ldap,ldapsession,w2lapp.cnf,w2lapp.core,w2lapp.gui, \
       w2lapp.schema.viewer

from w2lapp.session import session

from ldap.controls.simple import ValueLessRequestControl,BooleanControl

AVAILABLE_BOOLEAN_CONTROLS = {
  ldapsession.CONTROL_SUBENTRIES:           (('search_ext',),BooleanControl,True),
  ldapsession.CONTROL_LDUP_SUBENTRIES:      (('search_ext',),ValueLessRequestControl,None),
  ldapsession.CONTROL_MANAGEDSAIT:          (('**all**',),ValueLessRequestControl,None),
  ldapsession.CONTROL_RELAXRULES:           (('**write**',),ValueLessRequestControl,None),
  ldapsession.CONTROL_DONOTREPLICATE:       (('**write**',),ValueLessRequestControl,None),
  ldapsession.CONTROL_DONTUSECOPY:          (('**read**',),ValueLessRequestControl,None),
  ldapsession.CONTROL_DONTUSECOPY_OPENLDAP: (('**read**',),ValueLessRequestControl,None),
  ldapsession.CONTROL_SERVERADMINISTRATION: (('**write**',),ValueLessRequestControl,None),          # IBM DS
  '2.16.840.1.113730.3.4.17':               (('**read**',),ValueLessRequestControl,None),           # "real attributes only" control
  '2.16.840.1.113730.3.4.19':               (('**read**',),ValueLessRequestControl,None),           # "virtual attributes only" control
  '1.3.6.1.4.1.4203.666.11.9.5.1':          (('**all**',),ValueLessRequestControl,None),            # OpenLDAP's privateDB control for slapo-pcache
  '1.3.18.0.2.10.26':                       (('delete_ext','rename'),ValueLessRequestControl,None), # Omit group referential integrity control
  '1.2.840.113556.1.4.529':                 (('**read**',),ValueLessRequestControl,None),           # MS AD LDAP_SERVER_EXTENDED_DN_OID
  '1.2.840.113556.1.4.417':                 (('**all**',),ValueLessRequestControl,None),            # MS AD LDAP_SERVER_SHOW_DELETED_OID
  '1.2.840.113556.1.4.2064':                (('search_ext',),ValueLessRequestControl,None),         # MS AD LDAP_SERVER_SHOW_RECYCLED_OID
  '1.2.840.113556.1.4.1339':                (('search_ext',),ValueLessRequestControl,None),         # MS AD LDAP_SERVER_DOMAIN_SCOPE_OID
  '1.2.840.113556.1.4.2065':                (('search_ext',),ValueLessRequestControl,None),         # MS AD LDAP_SERVER_SHOW_DEACTIVATED_LINK_OID
  '1.3.6.1.4.1.42.2.27.9.5.2':              (('search_ext',),ValueLessRequestControl,None),         # Effective Rights control
  '1.3.6.1.4.1.26027.1.5.2':                (('**write**',),ValueLessRequestControl,None),          # Replication Repair Control
  '1.2.840.113556.1.4.619':                 (('**write**',),ValueLessRequestControl,None),          # MS AD LDAP_SERVER_LAZY_COMMIT_OID
}



# OID description dictionary from configuration directory
from ldapoidreg import oid as oid_desc_reg

##############################################################################
# LDAP connection parameters
##############################################################################

def w2l_LDAPParameters(sid,outf,command,form,ls,dn):

  protocol_version = ls.l.get_option(ldap.OPT_PROTOCOL_VERSION)

  # Set the LDAP connection option for deferencing aliases
  ldap_deref = form.getInputValue('ldap_deref',[])
  if ldap_deref:
    ls.l.deref = int(ldap_deref[0])

  context_menu_list = []

  w2lapp.gui.TopSection(
    sid,outf,command,form,ls,dn,
    'LDAP Connection Parameters',
    w2lapp.gui.MainMenu(sid,form,ls,dn),
    context_menu_list=context_menu_list
  )

  ldapparam_all_controls = form.getInputValue('ldapparam_all_controls',[u'0'])[0]==u'1'

  ldapparam_enable_control = form.getInputValue('ldapparam_enable_control',[None])[0]
  if ldapparam_enable_control and \
     ldapparam_enable_control in AVAILABLE_BOOLEAN_CONTROLS:
    methods,control_class,control_value = AVAILABLE_BOOLEAN_CONTROLS[ldapparam_enable_control]
    for method in methods:
      if control_value!=None:
        ls.l.add_server_control(method,control_class(ldapparam_enable_control,1,control_value))
      else:
        ls.l.add_server_control(method,control_class(ldapparam_enable_control,1))

  ldapparam_disable_control = form.getInputValue('ldapparam_disable_control',[None])[0]
  if ldapparam_disable_control and \
     ldapparam_disable_control in AVAILABLE_BOOLEAN_CONTROLS:
    methods,control_class,control_value = AVAILABLE_BOOLEAN_CONTROLS[ldapparam_disable_control]
    for method in methods:
      ls.l.del_server_control(method,ldapparam_disable_control)

  # Determine which controls are enabled
  enabled_controls = set()
  for control_oid,control_spec in AVAILABLE_BOOLEAN_CONTROLS.items():
    methods,control_class,control_value = control_spec
    control_enabled = True
    for method in methods:
      control_enabled = control_enabled and (control_oid in ls.l._get_server_ctrls(method))
    if control_enabled:
      enabled_controls.add(control_oid)

  # Prepare input fields for LDAPv3 controls
  control_table_rows = []
  for control_oid in AVAILABLE_BOOLEAN_CONTROLS.keys():
    control_enabled = (control_oid in enabled_controls)
    if not (control_enabled or ldapparam_all_controls or control_oid in ls.supportedControl):
      continue
    name,description,reference = oid_desc_reg[control_oid]
    control_oid_u = unicode(control_oid,'ascii')
    control_table_rows.append(
      """
      <tr>
        <td>%s</td>
        <td>%s%s%s</td>
        <td>%s</td>
        <td>%s</td>
      </tr>
      """ % (
        form.applAnchor(
          'ldapparams',
          {False:'Enable',True:'Disable'}[control_enabled],
          sid,
          [
            ('dn',dn),
            (
              'ldapparam_%s_control' % {False:u'enable',True:u'disable'}[control_enabled],
              control_oid_u
            ),
          ],
          title=u'%s %s' % (
            {False:u'Enable',True:u'Disable'}[control_enabled],
            name,
          ),
        ),
        {False:'<strike>',True:''}[control_oid in ls.supportedControl],
        form.utf2display(name),
        {False:'</strike>',True:''}[control_oid in ls.supportedControl],
        form.utf2display(control_oid_u),
        form.utf2display(description),
    ))

  outf.write("""
    <h1>LDAP Options</h1>
    <p>Jump to another entry by entering its DN:</p>
    %s
    <p>Alias dereferencing:</p>
    %s
    <h2>LDAPv3 extended controls</h2>
    <p>List %s controls</p>
    <table id="booleancontrolstable" summary="Simple boolean controls">
      <tr>
        <th>&nbsp;</th>
        <th>Name</th>
        <th>OID</th>
        <th>Description</th>
      </tr>
      %s
        </table>
      </fieldset>
  """ % (
    form.formHTML('read','Go to',sid,'GET',[],extrastr=form.field['dn'].inputHTML()),
    form.formHTML('ldapparams','Set alias deref',sid,'GET',[],extrastr=form.field['ldap_deref'].inputHTML(default=str(ls.l.deref))),
    form.applAnchor(
      'ldapparams',
      {False:'all',True:'only known'}[ldapparam_all_controls],
      sid,
      [
        ('dn',dn),
        ('ldapparam_all_controls',unicode(int(not ldapparam_all_controls))),
      ],
      title=u'Show %s controls' % (
        {False:u'all',True:u'known'}[ldapparam_all_controls],
      ),
    ),
    '\n'.join(control_table_rows),
  ))

  w2lapp.gui.Footer(outf,form)
