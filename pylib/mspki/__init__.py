"""
mspki - module package for parsing X.509 certs and CRLs
(c) by Michael Stroeder <michael@stroeder.com>

This module is distributed under the terms of the
GPL (GNU GENERAL PUBLIC LICENSE) Version 2
(see http://www.gnu.org/copyleft/gpl.html)

This module requires at least sub-module asn1.py of package Pisces
found on http://www.cnri.reston.va.us/software/pisces/
"""
